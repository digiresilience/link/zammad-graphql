FROM node:lts-buster-slim as builder
ARG APP_DIR=/app
RUN mkdir -p ${APP_DIR}
COPY package.json package-lock.json ${APP_DIR}/
RUN chown -R node:node ${APP_DIR}
USER node
WORKDIR ${APP_DIR}
RUN npm install
FROM node:lts-buster-slim

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
    dumb-init

ARG APP_DIR=/app
RUN mkdir -p ${APP_DIR}
RUN chown -R node:node ${APP_DIR}
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

USER node
WORKDIR ${APP_DIR}
COPY src src/
COPY test test/
COPY package.json package.json
COPY --from=builder ${APP_DIR}/node_modules node_modules

LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG VCS_URL="https://gitlab.com/digiresilience/link/zammad-graphql.git"
ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
LABEL org.label-schema.build-date="${BUILD_DATE}" \
      org.label-schema.name="zammad-graphql" \
      org.label-schema.license="AGPL-3.0" \
      org.label-schema.description="GraphQL server for zammad" \
      org.label-schema.url="https://digiresilience.org" \
      org.label-schema.vcs-url="${VCS_URL}" \
      org.label-schema.vcs-url="${VCS_REF}" \
      org.label-schema.vcs-type="git" \
      org.label-schema.vendor="CDR" \
      org.label-schema.schema-version="${VERSION}"

ENV PORT 3000
ENV NODE_ENV production
ENV APP_DIR ${APP_DIR}
ENTRYPOINT ["/docker-entrypoint.sh"]
