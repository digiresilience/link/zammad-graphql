PACKAGE_VERSION ?= $(shell cat package.json | grep version | head -1 | awk -F: '{ print $$2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')
BUILD_DATE   ?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
DOCKER_ARGS  ?=
DOCKER_NS    ?= registry.gitlab.com/digiresilience/link/zammad-graphql
DOCKER_TAG   ?= test
DOCKER_BUILD := docker build   ${DOCKER_ARGS}
DOCKER_BUILD_FRESH := ${DOCKER_BUILD} --pull --no-cache
DOCKER_BUILD_ARGS := --build-arg BUILD_DATE=${BUILD_DATE} --build-arg VERSION=${PACKAGE_VERSION} --build-arg VCS_REF=${CI_COMMIT_SHORT_SHA}
DOCKER_PUSH  := docker push
DOCKER_BUILD_TAG := ${DOCKER_NS}:${DOCKER_TAG}

.PHONY: build build-fresh push build-push build-fresh-push clean test fmt distclean lint

build:
	${DOCKER_BUILD} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

build-fresh:
	${DOCKER_BUILD_FRESH} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

push:
	${DOCKER_PUSH} ${DOCKER_BUILD_TAG}

build-push: build push
build-fresh-push: build-fresh push

add-tag:
	docker pull ${DOCKER_NS}:${DOCKER_TAG}
	docker tag ${DOCKER_NS}:${DOCKER_TAG} ${DOCKER_NS}:${DOCKER_TAG_NEW}
	docker push ${DOCKER_NS}:${DOCKER_TAG_NEW}

serve:
	npm run start

lint:
	npm run lint

test: lint
	npm run test

ci-test:
	cd test && docker-compose build && docker-compose up --quiet-pull --exit-code-from graphql

fmt:
	npm run fmt:all

clean:
	rm -rf coverage
	cd test && docker-compose down -v


distclean: clean
	rm -rf node_modules

prepare-commit: fmt lint test
