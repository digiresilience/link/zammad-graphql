# zammad-graphql

[![pipeline status][ci-img]][ci]

Connect's [postgraphile][postgraphile] to [Zammad][zammad]'s postgres database to create a GraphQL API.

## Deployment

### Environment variables

```sh
# Required
DATABASE_URL="postgresql://postgres:zammad@127.0.0.1:5432/zammad_dev"

# Optional
NODE_ENV=production
PORT=3000
HOST=127.0.0.1
DATABASE_SCHEMA=public
```

### Available docker tags

```
registry.gitlab.com/digiresilience/link/zamamd-graphql:master
registry.gitlab.com/digiresilience/link/zamamd-graphql:develop
```

## Development

### Prerequisites

For local development:

- node.js >= 12.x

### Run the thing

```console

cp env-sample .env
# edit .env
npm install
make serve

```

## License

[![License GNU AGPL v3.0][license-img]][license]

This is a free software project licensed under the GNU Affero General Public
License v3.0 (GNU AGPLv3) by [The Center for Digital Resilience][cdr] and
[Guardian Project][gp].

[license]: https://gitlab.com/digiresilience/link/zamamd-graphql/blob/master/LICENSE.md
[license-img]: https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg
[ci]: https://gitlab.com/digiresilience/link/zammad-graphql/-/commits/master
[ci-img]: https://gitlab.com/digiresilience/link/zammad-graphql/badges/master/pipeline.svg
[gp]: https://guardianproject.info
[cdr]: https://digiresilience.org
[zammad]: https://zammad.org
[postgraphile]: https://www.graphile.org
