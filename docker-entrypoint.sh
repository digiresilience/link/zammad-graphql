#!/bin/bash

set -e

cd "${APP_DIR}"

if [ "$1" = 'test' ]; then
  bash ./test/wait-until-ready.sh
  npm run test
else
  echo "starting zammad-graphql..."
  exec dumb-init node src/index.js
fi
