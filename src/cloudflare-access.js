const jwt = require("koa-jwt");
const jwks = require("jwks-rsa");
const ms = require("ms");
const { logger } = require("./logging");
const config = require("./config");

/**
 * Installs the cloudflare access JWT middleware into the koa app.
 *
 * @param app - the koa app object
 * @param {{url: String, audience: String, jwksUrl: String}}
 */
const cloudflareAccessInitializer = (app, { url, audience, jwksUrl }) => {
  logger.debug("cloudflare access initialized", { url, audience, jwksUrl });

  // initialize the jwt middleware using CF specific params
  const jwtMw = jwt({
    audience,
    issuer: url,
    cookie: "CF_Authorization",
    algorithms: ["RS256"],
    debug: true,
    secret: jwks.koaJwtSecret({
      jwksUri: jwksUrl,
      cache: true,
      cacheMaxEntries: 5,
      cacheMaxAge: ms("10h")
    })
  });
  app.use(async (ctx, next) => {
    if (config.isDev) {
      const { host } = ctx.request.header;
      const skipJwt =
        host.startsWith("localhost") || host.startsWith("127.0.0.1");
      if (!skipJwt) return jwtMw(ctx, next);
      logger.warn("SKIPPING JWT VERIFICATION in dev mode", { host });
      return next();
    }
    return jwtMw(ctx, next);
  });
};

module.exports = cloudflareAccessInitializer;
