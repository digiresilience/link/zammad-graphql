const path = require("path");
const dotenv = require("dotenv");
const dotenvExpand = require("dotenv-expand");
const convict = require("convict");

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "development";
  // eslint-disable-next-line no-console
  console.log("Config assuming development mode.");
}

if (process.env.NODE_ENV === "test") {
  dotenvExpand(
    dotenv.config({ path: path.resolve(process.cwd(), ".env-test") })
  );
} else if (process.env.NODE_ENV === "development") {
  dotenvExpand(dotenv.config());
} else {
  // we do not load env files in prod
}

const config = convict({
  env: {
    doc: "The application environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV"
  },
  zammad: {
    connection: {
      doc: "The Zammad postgresql connection string",
      format: String,
      default: null,
      env: "DATABASE_URL"
    },
    schema: {
      doc: "The postgresql schema zammad uses",
      default: "public",
      format: String,
      env: "DATABASE_SCHEMA"
    }
  },
  server: {
    host: {
      doc: "The IP address to bind to",
      format: "ipaddress",
      default: "127.0.0.1",
      env: "HOST"
    },
    port: {
      doc: "The port to bind.",
      format: "port",
      default: 3000,
      env: "PORT",
      arg: "port"
    }
  },
  logging: {
    level: {
      doc: "The logging level",
      format: String,
      default: "info",
      env: "LOG_LEVEL"
    }
  },
  cfaccess: {
    url: {
      doc: "The Cloudflare Access Login Page url, no trailing slash",
      format: "url",
      env: "CLOUDFLARE_ACCESS_URL",
      default: null
    },
    audience: {
      doc: "The Cloudflare Access audience id for this application",
      format: String,
      env: "CLOUDFLARE_ACCESS_AUDIENCE",
      default: null
    }
  }
});

const env = config.get("env");
config.isProd = env === "production";
config.isTest = env === "test";
config.isDev = env === "development";

// load config from config files if provided
if (config.isTest) {
  config.loadFile(`${process.env.PWD}/test/config.test.json`);
}

config.validate({ allowed: "strict" });
// add computed values
config.set(
  "cfaccess.jwksUrl",
  `${config.get("cfaccess.url")}/cdn-cgi/access/certs`
);

module.exports = config;
