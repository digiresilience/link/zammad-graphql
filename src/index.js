const Koa = require("koa");
const { postgraphile } = require("postgraphile");
const PgSimplifyInflector = require("@graphile-contrib/pg-simplify-inflector");
const { MutationPlugin } = require("graphile-build");
const PgBytea = require("./pg-bytea-plugin");
const config = require("./config");
const cloudflareAccess = require("./cloudflare-access");
const { logger, requestLogger } = require("./logging");
const requestLoggerMiddleware = require("./request-logger");

class App extends Koa {
  constructor(...params) {
    super(...params);
    this.servers = [];
  }

  listen(...args) {
    const server = super.listen(...args);
    this.servers.push(server);
    return server;
  }

  terminate() {
    this.servers.forEach(s => s.close());
  }
}

const app = new App();
app.logger = logger;

app.use(requestLoggerMiddleware(app, requestLogger, config.isProd));
cloudflareAccess(app, config.get("cfaccess"));
const postgraphileInstance = postgraphile(
  config.get("zammad.connection"),
  config.get("zammad.schema"),
  {
    watchPg: config.isDev,
    graphiql: true,
    dynamicJson: true,
    setofFunctionsContainNulls: false,
    ignoreRBAC: false,
    ignoreIndexes: true,
    retryOnInitFail: config.isProd,
    showErrorStack: config.isDev ? "json" : undefined,
    extendedErrors: config.isProd ? ["errcode"] : ["hint", "detail", "errcode"],
    appendPlugins: [PgSimplifyInflector, PgBytea],
    exportGqlSchemaPath: config.isDev ? "schema.graphql" : undefined,
    enhanceGraphiql: true,
    allowExplain: false,
    enableQueryBatching: true,
    legacyRelations: "omit",
    skipPlugins: [MutationPlugin],
    subscriptions: false
  }
);

app.use(postgraphileInstance);

const start = () => {
  const port = config.get("server.port");
  const host = config.get("server.host");
  app.logger.info(`zammad graphql server starting on ${host}:${port}`);
  app.listen(port, host);
};

if (!module.parent) start();

module.exports = { start, app, postgraphileInstance };
