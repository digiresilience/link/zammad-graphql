const winston = require("winston");
const logfmter = require("logfmt");
const R = require("ramda");
const chalk = require("chalk");
const config = require("./config.js");

const prodLogFmt = winston.format((info, opts) => {
  const msgs = [];
  if (info.method) msgs.push(`${chalk.bold(info.method.toUpperCase())}`);
  if (info.path) msgs.push(`${chalk.gray(info.path)}`);

  // eslint-disable-next-line no-param-reassign
  info.message = `${msgs.join(" ")}`;
  return info;
});

const logfmt = winston.format((info, opts) => {
  const props = R.omit(R.defaultTo([], opts.but), info);
  const infoPatched = info;
  infoPatched.props = logfmter.stringify(props);
  return infoPatched;
});

const logfmtPrintf = info =>
  `${info.timestamp} ${info.level} "${info.message}" ${info.props}`;

const logger = winston.createLogger({
  format: winston.format.combine(winston.format.timestamp()),
  level: config.get("logging.level"),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        logfmt({ but: ["level", "message", "timestamp"] }),
        winston.format.printf(logfmtPrintf)
      )
    })
  ]
});
const requestLogger = winston.createLogger({
  format: winston.format.combine(winston.format.timestamp()),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        prodLogFmt(),
        winston.format.splat(),
        logfmt({ but: ["statusColor", "level", "message", "timestamp"] }),
        winston.format.printf(logfmtPrintf)
      )
    })
  ],
  level: "info"
});

module.exports = { requestLogger, logger };
