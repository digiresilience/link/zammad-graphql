const { start, app, postgraphileInstance } = require("../src/index.js");

const exit = jest.spyOn(process, "exit").mockImplementation(() => {
  throw new Error("postgraphile exited prematurely");
});
describe("postgraphile server", () => {
  beforeAll(() => {
    start();
  });

  afterAll(() => {
    postgraphileInstance.pgPool.end();
    app.terminate();
  });
  test("it boots succesfully", async () => {
    const schema = await postgraphileInstance.getGraphQLSchema();
    expect(schema).toBeTruthy();
    expect(exit).toHaveBeenCalledTimes(0);
  });
});
