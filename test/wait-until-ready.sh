#!/usr/bin/env bash

POSTGRESQL_HOST="${POSTGRESQL_HOST:-zammad-postgresql}"
POSTGRESQL_PORT="${POSTGRESQL_PORT:-5432}"
POSTGRESQL_USER="${POSTGRESQL_USER:-postgres}"
POSTGRESQL_PASS="${POSTGRESQL_PASS:-}"
POSTGRESQL_DB="${POSTGRESQL_DB:-zammad_production}"

until (echo > /dev/tcp/"${POSTGRESQL_HOST}"/"${POSTGRESQL_PORT}") &> /dev/null; do
  echo "waiting for postgresql server to be ready..."
  sleep 5
done

echo "postgresql ready"

url="postgresql://${POSTGRESQL_USER}:${POSTGRESQL_PASS}@${POSTGRESQL_HOST}:${POSTGRESQL_PORT}/${POSTGRESQL_DB}"
until (psql "$url" -c "SELECT to_regclass('tickets');"|grep -qw tickets);do
  echo "waiting for zammad-init to create schema"
  sleep 5
done

sleep 5

echo "${POSTGRESQL_DB} ready"
